import React from 'react';
import { Provider } from 'react-redux';
import { NavigationContainer } from '@react-navigation/native';


import ScreenNavigator from './src/config/route/Stack';

const App = () => {
    return (
        // <Provider >
        <NavigationContainer>
            <ScreenNavigator />
        </NavigationContainer>
        // </Provider>
    )
}

export default App;