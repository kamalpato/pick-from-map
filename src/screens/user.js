import Axios from "axios";
import React, { Component } from "react";
import { View, Text, Image, ScrollView, TouchableOpacity, RefreshControl, ToastAndroid, Platform, Alert, StatusBar, TouchableHighlight, Modal, SafeAreaView } from "react-native";
import MapView, { Marker } from 'react-native-maps';
import Icon from "react-native-vector-icons/MaterialCommunityIcons";

const Person = require('../assets/main/Person.png');

class User extends Component {
    constructor(props) {
        super(props);
        this.state = {
            chooseIcon: false,
            refreshing: false,
            loading: false,
            visible: false,
            chooseIcon: false,
            Data: null,
            avatar: null,
            user: null,
            email: null,
            checkData: [],
            page: 1,
        }
        this.pressed = this.pressed.bind(this, true)
    }

    componentDidMount() {
        this.getDataUser()
        this.props.navigation.setOptions({
            headerRight: () => (
                <TouchableOpacity onPress={this.pressed()} >
                    {this.state.chooseIcon == true ?
                        <Icon name='map-marker' size={30} color='#2B637B' />
                        :
                        <Icon name='format-list-bulleted' size={30} color='#2B637B' />}
                </TouchableOpacity>)
        });
        console.log('choose', this.props.route.params)
    }

    pressed = (data) => {
        this.setState({ chooseIcon: data })
    }

    getDataUser = () => {
        try {
            this.setState({ loading: true })
            Axios({
                method: "GET",
                url: `https://reqres.in/api/users?page=${this.state.page}&amp;per_page=10`
            }).then(res => {
                if (this.state.page > 1) {
                    this.setState({ checkData: res.data.data, pushData: res.data, Data: [...this.state.Data, ...res.data.data], refreshing: false })
                } else { this.setState({ checkData: res.data.data, pushData: res.data, Data: res.data.data }) }
            })
        } catch (error) {
            this.setState({ loading: false, loading: false, refreshing: false }, ToastAndroid.show("Get data failed !", ToastAndroid.SHORT))

        }
    }

    _onRefresh = () => {
        if (this.state.Data.length == 0 || this.state.checkData.length == 0) {
            if (Platform.OS == 'android') {
                ToastAndroid.show("No more data !", ToastAndroid.SHORT)
            } else {
                Alert.alert("No more data !")
            }
        } else {
            this.setState({
                page: this.state.page + 1,
                refreshing: true,
            }, () => this.getDataUser(this.state.page));
        }
    }

    isCloseToBottom = ({ layoutMeasurement, contentOffset, contentSize }) => {
        const paddingToBottom = 20;
        return layoutMeasurement.height + contentOffset.y >=
            contentSize.height - paddingToBottom;
    };

    renderUsers = (data) => {
        return data?.map((item) => (
            <TouchableOpacity
                onPress={() => this.props.navigation.navigate("Home",
                    {
                        avatar: item.avatar,
                        username: item.first_name + " " + item.last_name,
                        email: item.email
                    }
                )} style={{ flex: 1, backgroundColor: '#FFF', width: '95%', alignSelf: 'center', alignItems: 'center', flexDirection: 'row', borderBottomWidth: 1, borderBottomColor: '#E2E3E4', paddingVertical: 15 }}>
                <Image source={{ uri: item.avatar }} style={{ height: 49, width: 49, borderRadius: 100 }} />
                <View style={{ marginLeft: 15 }}>
                    <Text style={{ fontFamily: 'Poppins-Medium', fontSize: 16, color: '#04021D' }}>{item.first_name + " " + item.last_name}</Text>
                    <Text style={{ fontFamily: 'Poppins-Medium', fontSize: 10, color: '#686777' }}>{item.email}</Text>
                </View>
            </TouchableOpacity>
        ))
    }

    modalUser = () => {
        return (
            <Modal
                visible={this.state.visible}
                transparent={true}
                animationType='slide'
            >
                <View style={{ flex: 1, justifyContent: 'flex-end', }}>
                    <View style={{ backgroundColor: "#FFF", borderTopLeftRadius: 20, borderTopRightRadius: 20, elevation: 2, }}>
                        <View style={{ alignItems: 'center', paddingTop: 10, }}>
                            <Icon name="close" onPress={() => this.setState({ visible: false })} size={20} color='#2B637B' style={{ alignSelf: 'flex-end', paddingTop: 10, paddingRight: 10 }} />
                            <Image source={{ uri: this.state.avatar }} style={{ height: 84, width: 84, marginBottom: 10, borderRadius: 100 }} />
                            <Text style={{ textAlign: 'center', color: '#626166', fontSize: 16, fontFamily: 'Poppins-Medium' }}>{this.state.user}</Text>
                        </View>
                        <TouchableOpacity onPress={() => this.setState({ visible: false }, this.props.navigation.navigate("Home", {
                            avatar: this.state.avatar,
                            username: this.state.user,
                            email: this.state.email
                        }))} style={{ backgroundColor: '#2B637B', marginHorizontal: 20, paddingVertical: 10, borderRadius: 12, marginTop: 45, marginBottom: 20 }}>
                            <Text style={{ textAlign: 'center', color: '#FFF', fontSize: 14, fontFamily: 'Poppins-Medium' }}>Select</Text>
                        </TouchableOpacity>
                    </View>
                </View>
            </Modal>
        )
    }

    coordinate = [
        {
            latitude: -6.279569542780388,
            longitude: 106.82027891514075
        },
        {
            latitude: -6.270912389515023,
            longitude: 106.82136758224554
        },
        {
            latitude: -6.275912094885713,
            longitude: 106.82457392436585
        },
        {
            latitude: -6.2715737049071,
            longitude: 106.81652906177978
        },
        {
            latitude: -6.288061292804747,
            longitude: 106.81763486961559
        },
        {
            latitude: -6.270912389515023,
            longitude: 106.82136758224554
        },
        {
            latitude: -6.278750719100015,
            longitude: 106.81945620016873
        },
        {
            latitude: -6.28476381700461,
            longitude: 106.83305113179743
        },
        {
            latitude: -6.279526606603581,
            longitude: 106.83942578873337
        },
        {
            latitude: -6.264137955367091,
            longitude: 106.84501987543226
        },
        {
            latitude: -6.267943187246048,
            longitude: 106.84973945262398
        },
        {
            latitude: -6.261531616154091,
            longitude: 106.84596379087058
        },
    ]

    render() {
        const { Data } = this.state;
        const newData = Data?.map((item, index) => ({ ...item, latitude: this.coordinate[index].latitude, longitude: this.coordinate[index].longitude }))
        // console.log("insert array", newData)
        return (
            <SafeAreaView style={{ flex: 1 }}>
                <View style={{ flex: 1, backgroundColor: '#FFF' }}>
                    <StatusBar translucent backgroundColor='transparent' />
                    <View style={{ height: 80,  borderBottomWidth: 1, borderBottomColor: '#E2E3E4', elevation: 2 }}>
                        <View style={{ marginTop: 35, flexDirection: 'row', alignItems: 'center', justifyContent: 'space-between', }}>
                            <TouchableOpacity style={{ marginLeft: 20 }} onPress={() => this.props.navigation.goBack()}>
                                <Icon name='chevron-left' size={30} color='#2B637B' />
                            </TouchableOpacity>
                            <View>
                                <Text style={{
                                    fontFamily: 'Poppins-Medium',
                                    fontSize: 18,
                                    color: '#2B637B'
                                }}>Users</Text>
                            </View>
                            <TouchableOpacity style={{ marginRight: 20 }} onPress={() => this.setState({ chooseIcon: !this.state.chooseIcon })}>
                                {this.state.chooseIcon == true ?
                                    <Icon name='map-marker' size={30} color='#2B637B' />
                                    :
                                    <Icon name='format-list-bulleted' size={30} color='#2B637B' />}
                            </TouchableOpacity>
                        </View>
                    </View>
                    {this.modalUser()}
                    {this.state.chooseIcon == true ?
                        <ScrollView
                            refreshControl={
                                <RefreshControl refreshing={this.state.refreshing}
                                    onRefresh={this._onRefresh} />
                            }
                            onScroll={({ nativeEvent }) => {
                                if (this.isCloseToBottom(nativeEvent)) {
                                    this._onRefresh();
                                }
                            }}
                            scrollEventThrottle={400}
                            keyboardShouldPersistTaps={'handled'}
                            showsVerticalScrollIndicator={false}>
                            {this.renderUsers(Data)}
                        </ScrollView> :
                        <MapView
                            style={{ flex: 1 }}
                            initialRegion={{
                                latitude: -6.2762427,
                                longitude: 106.8224496,
                                latitudeDelta: 0.0122,
                                longitudeDelta: 0.0421,
                            }}>
                            {newData?.map((item) => (
                                <View>
                                    <Marker
                                        onPress={() => this.setState({ visible: true, avatar: item.avatar, user: item.first_name + " " + item.last_name, email: item.email })}
                                        coordinate={{ latitude: item.latitude, longitude: item.longitude }}
                                    />
                                    {/* {console.log("latitude", item.latitude)} */}
                                </View>
                            ))}
                        </MapView>}
                </View >
            </SafeAreaView>
        )
    }
}

export default User;