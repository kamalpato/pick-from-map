import React, { Component } from "react";
import { View, Text, TouchableOpacity, SafeAreaView, KeyboardAvoidingView, Image } from "react-native";

// Image
const Person = require('../assets/main/Person.png');

class Home extends Component {
    constructor(props) {
        super(props);
        this.state = {

        }
    }
    render() {
        const navigation = this.props.navigation;
        console.log("Passing Params", this.props.route.params)
        return (
            <KeyboardAvoidingView
                behavior={Platform.OS === "ios" ? "padding" : null}
                style={{ flex: 1 }}
            >
                <SafeAreaView style={{ flex: 1 }}>
                    <View style={{ width: '90%', alignSelf: 'center' }}>
                        <Text style={{ color: '#04021D', fontSize: 12, fontFamily: 'Poppins-Medium', marginTop: 20 }}>Welcome</Text>
                        <Text style={{ color: '#04021D', fontSize: 18, fontFamily: 'Poppins-Medium' }}>{this.props.route.params.names ? this.props.route.params.names : "John Doe"}</Text>
                    </View>
                    {this.props.route.params.avatar ?
                        <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
                            <Image source={{ uri: this.props.route.params.avatar }} style={{ height: 164, width: 164, marginBottom: 35, borderRadius: 100 }} />
                            <Text style={{ textAlign: 'center', color: '#626166', fontSize: 24, fontFamily: 'Poppins-Medium' }}>{this.props.route.params.username}</Text>
                            <Text style={{ textAlign: 'center', color: '#808080', fontSize: 18, fontFamily: 'Poppins-Medium' }}>{this.props.route.params.email}</Text>
                            <Text
                                onPress={() => this.props.navigation.navigate("WebView", {
                                    link: "https://suitmedia.com/"
                                })}
                                style={{ textAlign: 'center', color: '#2B637B', fontSize: 18, fontFamily: 'Poppins-Medium', textDecorationLine: 'underline' }}>website</Text>
                        </View> :
                        <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
                            <Image source={Person} style={{ height: 164, width: 164, marginBottom: 35 }} />
                            <Text style={{ textAlign: 'center', color: '#808080', fontSize: 18, fontFamily: 'Poppins-Medium' }}>Select a user to show the profile</Text>
                        </View>}
                    <TouchableOpacity onPress={() => navigation.navigate("Users")} style={{ backgroundColor: '#2B637B', marginHorizontal: 20, paddingVertical: 10, borderRadius: 12, marginTop: 45 }}>
                        <Text style={{ textAlign: 'center', color: '#FFF', fontSize: 14, fontFamily: 'Poppins-Medium' }}>Choose a User</Text>
                    </TouchableOpacity>
                </SafeAreaView>
            </KeyboardAvoidingView>
        )
    }
}

export default Home;