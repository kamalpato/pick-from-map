import React, { Component } from "react";
import {
    View,
    Text,
    ImageBackground,
    SafeAreaView,
    KeyboardAvoidingView,
    StatusBar,
    TextInput,
    TouchableOpacity,
    Image,
    Alert
} from "react-native";

// Background
const BackGround = require('../assets/background/background.png');

// Image
const Profile = require('../assets/main/profile.png');

class Login extends Component {
    constructor(props) {
        super(props);
        this.state = {
            userName: '',
            Palindrome: ''
        }
    }

    palindromeCheck = () => {
        const { Palindrome } = this.state;
        if (Palindrome == "kasur rusak" || Palindrome == "step on no pets" || Palindrome == "put it up") {
            Alert.alert("isPalindrome")
        }
        else if (Palindrome == "suitmedia") { Alert.alert("not Palindrome") }
    }

    onLogin = () => {
        const { userName, Palindrome } = this.state;
        const navigation = this.props.navigation;
        if (userName && Palindrome == "kasur rusak" || Palindrome == "step on no pets" || Palindrome == "put it up") {
            navigation.replace('Home', { names: userName })
            // console.log('params', names)
        } else {
            Alert.alert("Please fill your Name or right Palindrome")
        }
    }

    render() {
        const { userName, Palindrome } = this.state;
        return (
            <ImageBackground source={BackGround} style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
                <KeyboardAvoidingView>
                    <SafeAreaView>
                        <StatusBar translucent backgroundColor='transparent' />
                        <View style={{ justifyContent: 'center', }}>
                            <Image source={Profile} style={{ height: 116, width: 116, marginBottom: 59, alignSelf: 'center' }} />
                            <TextInput
                                placeholder='Name'
                                style={{ backgroundColor: '#FFF', fontFamily: 'Poppins-Medium', paddingLeft: 15, borderRadius: 12, }}
                                value={userName}
                                onChangeText={(userName) => this.setState({ userName })}
                                autoCapitalize='none'
                            />
                            <TextInput
                                placeholder='Palindrome'
                                style={{ backgroundColor: '#FFF', fontFamily: 'Poppins-Medium', paddingLeft: 15, borderRadius: 12, marginTop: 23 }}
                                value={Palindrome}
                                onChangeText={(Palindrome) => this.setState({ Palindrome })}
                                autoCapitalize='none'
                            />
                            <TouchableOpacity onPress={() => this.palindromeCheck()} style={{ backgroundColor: '#2B637B', paddingHorizontal: 150, paddingVertical: 10, borderRadius: 12, marginTop: 45 }}>
                                <Text style={{ textAlign: 'center', color: '#FFF', fontSize: 14, fontFamily: 'Poppins-Medium' }}>CHECK</Text>
                            </TouchableOpacity>
                            <TouchableOpacity onPress={() => this.onLogin()} style={{ backgroundColor: '#2B637B', paddingHorizontal: 150, paddingVertical: 10, borderRadius: 12, marginTop: 15 }}>
                                <Text style={{ textAlign: 'center', color: '#FFF', fontSize: 14, fontFamily: 'Poppins-Medium' }}>NEXT</Text>
                            </TouchableOpacity>
                        </View>
                    </SafeAreaView>
                </KeyboardAvoidingView>
            </ImageBackground>
        )
    }
}

export default Login;