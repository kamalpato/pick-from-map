import React, { Component } from "react";
import { View, Text, StatusBar } from "react-native";
import { WebView } from "react-native-webview";

class Webview extends Component {
    constructor(props) {
        super(props);
        this.state = {

        }
    }

    // componentDidMount() {
    //     this.props.route.params.link
    // }

    render() {
        // console.log("link", this.props.route.params.link)
        return (
            <>
                <StatusBar translucent backgroundColor='transparent' />
                <WebView
                    containerStyle={{ flex: 1 }}
                    style={{ height: 315, borderRadius: 20, }}
                    javaScriptEnabled={true}
                    domStorageEnabled={true}
                    startInLoadingState={true}
                    scalesPageToFit={true}
                    androidHardwareAccelerationDisabled={true}
                    source={{ uri: 'https://suitmedia.com/' }}
                />
            </>
        )
    }
}

export default Webview;