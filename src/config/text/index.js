import { StyleSheet } from 'react-native';
import color from './colors';
import sizes from './sizes';

export const TextStyle = StyleSheet.create({
    logo: {
        fontSize: 24 * sizes.ratioWidthScreen,
        color: color.main,
        fontWeight: '400',
        fontFamily: 'Poppins-Medium',
        lineHeight: 36 * sizes.ratioWidthScreen,
        marginBottom: 10
    },
    button: {
        fontSize: 16 * sizes.ratioWidthScreen,
        color: color.main,
        fontWeight: '500',
        fontFamily: 'Poppins-Medium',
        textAlign: 'center',
        textAlignVertical: 'center',
        lineHeight: 24 * sizes.ratioWidthScreen,
    },
    normalWhite: {
        fontSize: 14 * sizes.ratioWidthScreen,
        color: color.white,
        fontWeight: '400',
        fontFamily: 'Roboto-Medium',
        lineHeight: 15.13 * sizes.ratioWidthScreen,
    },
    buttonSeparator: {
        fontSize: 16 * sizes.ratioWidthScreen,
        color: color.main,
        fontWeight: '500',
        fontFamily: 'Poppins-Medium',
        textAlign: 'center',
        textAlignVertical: 'center',
        marginVertical: 10,
        lineHeight: 24 * sizes.ratioWidthScreen,
    },
    OTPTitle: {
        fontSize: 14 * sizes.ratioWidthScreen,
        color: color.black,
        fontWeight: '400',
        fontFamily: 'Roboto-Medium',
        lineHeight: 15.13 * sizes.ratioWidthScreen,
        textAlign: 'center',
        paddingBottom: 10
    },
    OTPContent: {
        fontSize: 11 * sizes.ratioWidthScreen,
        color: color.black,
        fontWeight: '400',
        fontFamily: 'Roboto-Medium',
        lineHeight: 15.13 * sizes.ratioWidthScreen,
        textAlign: 'center'
    },
    OTPCount: {
        fontSize: 6 * sizes.ratioWidthScreen,
        color: color.black,
        fontWeight: '400',
        fontFamily: 'Roboto-Medium',
        lineHeight: 15.13 * sizes.ratioWidthScreen,
        textAlign: 'center',
    },
    PINTitle: {
        fontSize: 14 * sizes.ratioWidthScreen,
        color: color.black,
        fontWeight: '400',
        fontFamily: 'Roboto-Medium',
        lineHeight: 15.13 * sizes.ratioWidthScreen,
        textAlign: 'center',
    },
    PINDesc: {
        fontSize: 6 * sizes.ratioWidthScreen,
        color: color.black,
        fontWeight: '400',
        fontFamily: 'Roboto-Medium',
        lineHeight: 15.13 * sizes.ratioWidthScreen,
        textAlign: 'center',
    },
});
