import { Dimensions, StyleSheet } from 'react-native';

const { width, height } = Dimensions.get('window');

export default {
    widthScreen: width,
    heightScreen: height,
    ratioWidthScreen: width / 414,
    ratioHeightScreen: height / 754,
    borderWidth: StyleSheet.hairlineWidth,
};
