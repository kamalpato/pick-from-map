export default colors = {
  main: '#6E00FF',
  black: '#000',
  deadPurple: '#DBDBFB',
  grey: '#A1A4B2',
  red: '#EC4646',
  white: '#FFFFFF',
};
