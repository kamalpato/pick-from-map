import React, { Component } from 'react';
import { createAppContainer, createSwitchNavigator } from 'react-navigation';
import { createNativeStackNavigator } from '@react-navigation/native-stack';
import { useNavigation } from '@react-navigation/native';

import Login from '../../../screens/login';
import Home from '../../../screens/home';
import WebView from '../../../screens/webview';
import User from '../../../screens/user';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import { TouchableOpacity } from 'react-native';


const Stack = createNativeStackNavigator();

class Main extends Component {
    constructor(props) {
        super(props);
        this.state = {
            chooseIcon: false
        };
    }

    render() {
        // const navigation = useNavigation();
        // const [chooseIcon, setChooseIcon] = useState(false)
        // console.log('navi', navigation.setParams({ choose: chooseIcon }))
        // this.props.navigation.setOptions({ headerRight: chooseIcon });
        return (
            <Stack.Navigator initialRouteName={Login} headerMode='screen'>
                <Stack.Screen name="Login" component={Login} options={{ headerShown: false }} />
                <Stack.Screen name='Home' component={Home}
                    options={{
                        headerShown: true,
                        headerTitleStyle: {
                            fontFamily: 'Poppins-Medium',
                            fontSize: 18,
                            color: '#2B637B'
                        },
                        headerTitleAlign: 'center',
                        headerLeft: () => (
                            <TouchableOpacity onPress={() => navigation.goBack()}>
                                <Icon name='chevron-left' size={30} color='#2B637B' />
                            </TouchableOpacity>
                        )
                    }}
                />
                <Stack.Screen name='WebView' component={WebView}
                    options={{
                        headerShown: true,
                        title: null,
                        headerLeft: () => (
                            <TouchableOpacity onPress={() => navigation.goBack()}>
                                <Icon name='chevron-left' size={30} color='#2B637B' />
                            </TouchableOpacity>
                        ),
                    }}
                />
                <Stack.Screen name='Users' component={User}
                    options={({ navigation, route }) => (
                        {
                            headerShown: false,
                            headerTitleStyle: {
                                fontFamily: 'Poppins-Medium',
                                fontSize: 18,
                                color: '#2B637B'
                            },
                            headerTitleAlign: 'center',
                            headerLeft: () => (
                                <TouchableOpacity onPress={() => navigation.goBack()}>
                                    <Icon name='chevron-left' size={30} color='#2B637B' />
                                </TouchableOpacity>
                            ),
                            // headerRight: () => (
                            // props => <TouchableOpacity {...props} />
                            // headerRight: route.params?.headerRights
                            // <TouchableOpacity onPress={chooseIcon}>
                            //     {this.state.chooseIcon == true ?
                            //         <Icon name='map-marker' size={30} color='#2B637B' />
                            //         :
                            //         <Icon name='format-list-bulleted' size={30} color='#2B637B' />}
                            // </TouchableOpacity>),
                        })}
                />
            </Stack.Navigator>
        )
    }
}

const AppNavigator = createSwitchNavigator(
    {
        Main: Main,
    },
    {
        mode: 'modal',
        headerMode: 'none',
    }
);

const AppContainer = createAppContainer(AppNavigator)

export default () => {
    const prefix = 'http://192.168.1.12:5000'
    return <AppContainer uriPrefix={prefix} />
}