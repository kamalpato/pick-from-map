import React from "react";
import { TouchableOpacity, View, } from "react-native";
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';


const RightComponent = (chooseIcon) => {
    return (
        <>
            <TouchableOpacity onPress={chooseIcon}>
                {chooseIcon == true ?
                    <Icon name='map-marker' size={30} color='#2B637B' />
                    :
                    <Icon name='format-list-bulleted' size={30} color='#2B637B' />}
            </TouchableOpacity>
        </>
    )
}

export default RightComponent;